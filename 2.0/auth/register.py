import csv
import pwinput

def register():
    print(">","="*102,"<")
    print('|                                          REGISTRATION FORM                                             |')
    print(">","="*102,"<")
    username = input('|                                      Enter your username: ')
    password = pwinput.pwinput(prompt='|                                       Enter your password: ', mask='*')
    print(">","="*102,"<")
    enterr = input("==> Press Enter to Continue...")
    with open('database.csv', 'a') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([username, password])
    return True
