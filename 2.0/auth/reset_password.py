import csv
import pwinput
import time

def reset_password():
    print(">","="*102,"<")
    print('|                                          RESET PASSWORD FORM                                           |')
    print('|                                              Login First                                               |')
    print(">","="*102,"<")
    username = input('|                                      Enter your username: ')
    password = pwinput.pwinput(prompt='|                                       Enter your password: ', mask='*')
    with open('database.csv') as csv_file:
        reader = csv.reader(csv_file)
        is_success = False
        counter = 0
        index = None
        new_list = []
        for row in reader:
            new_list.append(row)
            if len(row) > 1:
                if row[0] == username and row[1] == password:
                    is_success = True
                    index = counter
            counter += 1
        
        if is_success:
            print(">","="*102,"<")
            new_password = input('|                                       Input New Password: ')
            print(">","="*102,"<")
            enterr = input("==> Press Enter to Continue...")
            for i in range(len(new_list)):
                if i == index:
                    new_list[i][1] = new_password
            with open('database.csv', 'w+') as csv_file:
                writer = csv.writer(csv_file)
                for i in range(len(new_list)):
                    writer.writerow(new_list[i])
            time.sleep(20)
        else:
            print('Login Failed')
        global is_exit
        global main_menu_input
        is_exit = True
        main_menu_input = ''
        # time.sleep(10)
