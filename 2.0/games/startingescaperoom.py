import os

def escape_starting():
    
        os.system('cls')
        print(">","="*107,"<")
        print("|  ________   ________   ________   _______   _______   ________     _______   _______   _______   ___   ___  |")
        print("| /   _____| |     ___| /   _____| |   _   | |   _   | /   _____|   |   _   | /   _   \ /   _   \ /   \_/   \ |")
        print("| |  |____   |    |___  |  |       |  | |  | |  |_|  | |  |____     |  |_|  | |  | |  | |  | |  | |         | |")
        print("| |   ____|  |__      | |  |       |  |_|  | |   ____| |   ____|    |      _| |  | |  | |  | |  | |  _   _  | |")
        print("| |  |_____   __|     | |  |_____  |   _   | |  |      |  |_____    |  |\  \  |  |_|  | |  |_|  | | | | | | | |")
        print("| \________| |________| \________| |__| |__| |__|      \________|   |__| |__| \_______/ \_______/ \_/ \_/ \_/ |")
        print("|","                                                                                                            |")
        print(">","="*107,"<")
        
        inputStart = input("| => Should we should start?(y or n) ")

        if(inputStart == "y" or "Y"):  
            print("|","")
            print("|","="*102,"|")
            print("|","| Instruction:                                                                                       |","|")
            print("|","|   You goal is to get the key and exit the room. You will see the controls or what you need inside  |","|") 
            print("|","|                                           the game                                                 |","|")
            print("|","|                                                                                                    |","|")
            print("|","="*102,"|")

            understandable = input("| => Is that clear?(y or n) ")

            if understandable == "y" or "Y":
                import games.escaperoom as escaperoom
                escaperoom()

            elif(inputStart == "n" or "N"):
                print("|","")
                print("|","="*102)
                print("|","                                   == That's ok, take your time :)) ==")
                print("|","="*102)
                print("|","")

                import menu
                menu.menu()

            else:
                print("|","")
                print("|","="*102)
                print("|","                           == I think that's not the answer I'm looking for :(( ==")
                print("|","="*102)
                    
                import games.escaperoom as escaperoom
                escaperoom()

        elif(inputStart == "n" or "N"):
            print("|","")
            print("|","="*102)
            print("|","                                   == That's ok, take your time :)) ==")
            print("|","="*102)
            print("|","")

            import menu
            menu.menu()

        else:
            print("|","")
            print("|","="*102)
            print("|","                           == I think that's not the answer I'm looking for :(( ==")
            print("|","="*102)
            
            import games.escaperoom as escaperoom
            escaperoom()