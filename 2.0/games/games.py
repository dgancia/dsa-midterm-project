import os
def clear():
    os.system('clear||cls') 

def allgames():

    os.system('cls')
    print("> =================================================================== <")
    print("|    _________    ________    ____________    ________    ________    |")
    print("|   /   ______|  |   __   |  /   _    _   \  /   _____|  |     ___|   |")
    print("|   |  |  ____   |  |  |  |  |  | |  | |  |  |  |____    |    |___    |")
    print("|   |  | |__  |  |  |__|  |  |  | |  | |  |  |   ____|   |__      |   |")
    print("|   |  |___|  |  |   __   |  |  | |  | |  |  |  |_____    __|     |   |")
    print("|   \_________|  |__|  |__|  |__| |__| |__|  \________|  |________|   |")
    print("|                                                                     |")
    print("> =================================================================== <")
    print(" ")
    print("> ------------------------------------------------------------------- <")
    print("|                         [1] Tic-Tac-Toe                             |")
    print("|                         [2] Escape Room                             |")
    print("|                         [3] 2 Pics 1 Word                           |")
    print("|                         [4] Go back to menu                         |")
    print("> ------------------------------------------------------------------- <")

    user_input = input("==> Choice: ")

    if user_input == "1":
        from games.tictactoe import game_start
        game_start()
    
    elif user_input == "2":
        from games.startingescaperoom import escape_starting
        escape_starting()
    
    elif user_input == "3":
        from games.word_games import gameStart
        gameStart()

    elif user_input == "4":
        clear()
        import menu
        menu.menu()

    else:
        print("> =================================================================== <")
        print("|       == I think that's not the answer I'm looking for :(( ==       |")
        print("> =================================================================== <")
        allgames()

    allgames()
