from games import word_images
import os

def gameStart():

    os.system('cls')

    print(">","="*107,"<")
    print("|   ________     _______   __    ________   ________      ____     _   _   _   _______   _______   _______    |")
    print("|  |     ___|   |   _   | |  |  /   _____| |     ___|    /_   |   | | | | | | /   _   \ |   _   | |   _   \   |")
    print("|  |    |       |  |_|  | |  |  |  |       |    |___       |  |   | | | | | | |  | |  | |  |_|  | |  | |   |  |")
    print("|  |__  |___    |   ____| |  |  |  |       |__      |      |  |   | | | | | | |  | |  | |      _| |  | |   |  |")
    print("|   __|     |   |  |      |  |  |  |_____   __|     |      |  |   | |_| |_| | |  |_|  | |  |\  \  |  |_|   |  |")
    print("|  |________|   |__|      |__|  \________| |________|      |__|   \_________/ \_______/ |__| |__| |_______/   |")
    print("|","                                                                                                            |")
    print(">","="*107,"<")

    inputStart = input("| => Should we should start?(y or n) ")
    

    if(inputStart == "y" or "Y"):  
        print("|","")
        print("|","="*102,"|")
        print("|","| Instruction:                                                                                       |","|")
        print("|","|     It will show two images and what you to do is to guess the hidden word you the given images    |","|") 
        print("|","|                    To answer you must enter the guessed word in the blank                          |","|")
        print("|","|                                                                                                    |","|")
        print("|","|                    ( !! WARNING: YOU ONLY HAVE A ONE CHANCE TO ANSWER !! )                         |","|")
        print("|","="*102,"|")
        understandable = input("| => Is that clear?(y or n) ")

        if(understandable == "y" or "Y"):

            os.system('cls')
            print(word_images.ballpen())
            player_answer = input("==> ")
                        
            if player_answer == "ballpen":
                print("")
                print("Right answer!, Next question")
                print(word_images.carpet())
                player_answer = input("==> ")

                if player_answer == 'carpet':
                    print("")
                    print("Right answer!, Next question")
                    print(word_images.cupcake())
                    player_answer = input("==> ")

                    if player_answer == "cupcake":
                        print("")
                        print("Right answer!, Next question")
                        print(word_images.facebook())
                        player_answer = input("==> ")

                        if player_answer == "facebook":
                            print("")
                            print("Right answer!, Next question")
                            print(word_images.congrats())
                            player_answer = input("Press enter to go back to menu... ")
                            from games.games import allgames
                            allgames()

                        elif (player_answer == "exit"):
                            os.system('cls')
                            import menu
                            menu.menu()

                        else:
                            print("")
                            print(word_images.lose())
                            player_answer = input("Press enter to try again... ")
                            gameStart()

                    elif (player_answer == "exit"):
                        os.system('cls')
                        import menu
                        menu.menu()

                    else:
                        print("")
                        print(word_images.lose())
                        player_answer = input("Press enter to try again... ")
                        gameStart()

                elif (player_answer == "exit"):
                    os.system('cls')
                    import menu
                    menu.menu()

                else:
                    print("")
                    print(word_images.lose())
                    player_answer = input("Press enter to try again... ")
                    gameStart()
            
            elif (player_answer == "exit"):
                    os.system('cls')
                    import menu
                    menu.menu()  

            else:
                print("")
                print(word_images.lose())
                player_answer = input("Press enter to try again... ")
                gameStart()

        elif (understandable == "exit"):
            os.system('cls')
            import menu
            menu.menu()

        elif (understandable == "n" or "N"):
            print("|","")
            print("|","="*102)
            print("|","                                   == That's ok, take your time :)) ==")
            print("|","="*102)
            print("|","")
            
            exit_input = input("==> Press enter to back to games...")
            gameStart()

        else:
            print("|","")
            print("|","="*102)
            print("|","                           == I think that's not the answer I'm looking for :(( ==")
            print("|","="*102)
            gameStart()


    elif (inputStart == "n" or "N"):
        print("|","")
        print("|","="*102)
        print("|","                                   == That's ok, take your time :)) ==")
        print("|","="*102)
        print("|","")
        
        exit_input = input("==> Press enter to back to games...")
        from games.games import allgames
        allgames()

    elif (inputStart == "exit"):
        os.system('cls')
        import menu
        menu.menu()

    else:
        print("|","")
        print("|","="*102)
        print("|","                           == I think that's not the answer I'm looking for :(( ==")
        print("|","="*102)
        gameStart()

