import os

def allgames():

    os.system('cls')
    print("> =================================================================== <")
    print("|    _________    ________    ____________    ________    ________    |")
    print("|   /   ______|  |   __   |  /   _    _   \  /   _____|  |     ___|   |")
    print("|   |  |  ____   |  |  |  |  |  | |  | |  |  |  |____    |    |___    |")
    print("|   |  | |__  |  |  |__|  |  |  | |  | |  |  |   ____|   |__      |   |")
    print("|   |  |___|  |  |   __   |  |  | |  | |  |  |  |_____    __|     |   |")
    print("|   \_________|  |__|  |__|  |__| |__| |__|  \________|  |________|   |")
    print("|                                                                     |")
    print("> =================================================================== <")
    print(" ")
    print("> ------------------------------------------------------------------- <")
    print("|                         [1] Tic-Tac-Toe                             |")
    print("|                         [2] Escape Room                             |")
    print("|                         [3] 2 Pics 1 Word                           |")
    print("|                         [4] Go back to menu                         |")
    print("> ------------------------------------------------------------------- <")

    user_input = input("==> Choice: ")

    if user_input == "1":
        import tictactoe
        tictactoe.gameStart()
    
    elif user_input == "2":
        import startingescaperoom
        startingescaperoom.escape_starting()
    
    elif user_input == "3":
        import word_games
        word_games.gameStart()

    elif user_input == "4":
        import main
        main.login_main()

    else:
        print("> =================================================================== <")
        print("|       == I think that's not the answer I'm looking for :(( ==       |")
        print("> =================================================================== <")
        allgames()

    allgames()
